---
title:      "Plasma Mobile at FOSSCOMM 2018"
created_at: 2018-10-31 11:30:00 UTC+2
author:     Dimitris Kardarakos
layout:     post
---
<img style="float: right;" alt="FOSSCOMM 2018" src="/img/fscm2018-logo.gif" width="25%">

On the 13th and the 14th of October, we visited Heraklion, Greece and participated in [FOSSCOMM 2018](https://fosscomm2018.gr/index.php/front-page-en/). FOSSCOMM is an annual conference about free software organized by free software enthusiasts and contributors that live in Greece. This year’s event took place in the University of Crete, in Heraklion, organized by the Radio Station of the University of Crete, the Graduate Students Union of the Computer Science Department and the toLABaki hackerspace. 

No doubt a foss conference is the perfect place to meet old friends, strengthen community and socials bonds, talk with interesting persons and make new friendships. But apart from "socializing and networking”, we also gave a talk on Saturday about free software on mobile devices and, in particular, Plasma Mobile.

In general, FOSSCOMM has been a great event, hosting over 60 talks and workshops, covering a wide range of interesting topics. The conference provided a great opportunity to foss contributors, university students and many others to attend talks about Linux desktop, privacy, politics, even about satellites. Moreover, the organizers emphasized on holding an event full of community spirit and free software values. Thanks to the efforts of the FOSSCOMM volunteers and organizers, the event has been quite successful in terms of content, participation and vibes.

<figure style="float: left; padding: 1ex; margin-right: 1ex;"><img alt="KDE el" src="/img/kde-el.png" width="400"></figure>

In our [presentation](https://share.kde.org/s/QAfe8fQFqPFbRFD/download), we talked on the importance of working on mobile phones that run free software and we presented the suggestion of KDE. At first, we tried to explain the correlation between free software on mobile and users’ rights and make clear that the proprietary mobile platforms do not fit our needs. After discussing on the technical factors that result to a difficult environment to work in, we talked on our suggestion for a complete and open software system. We also introduced Kirigami to the audience, the KDE framework that makes us dream of a touch friendly Linux application ecosystem. Finally, we talked a little bit about [Halium](https://halium.org/), the project that creates a common ground to interact with Android as well as about [postmarketOS](https://postmarketos.org/) and its goal to provide a touch optimized free Linux distribution based on mainline kernel.

Many people attended our talk asking several interesting questions about Plasma Mobile and, in general, about free software on mobile. Although we had already talked a lot about collaboration with other communities during the presentation, many of the audience questions had a lot to do with our relationship with communities and device manufacturers. In our opinion, this fact stresses the importance of cooperation, knowledge sharing and coordination between mobile foss initiatives.

All these make us think that we managed to raise awareness of the importance of free software on mobile as well as of the value of [contributing](/findyourway) to Plasma Mobile. See you at FOSSCOMM 2019!

<figure style="padding: 1ex; 1ex; border: 1px solid grey; text-align: center;"><a href="/img/fosscomm_volunteers.jpg"><img alt="Our presentation" src="/img/fosscomm_volunteers.jpg" width="900" heigh="508"></a><figcaption>FOSSCOMM volunteers and organizers</figcaption></figure>
