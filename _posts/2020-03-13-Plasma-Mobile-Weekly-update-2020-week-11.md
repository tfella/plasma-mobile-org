---
title:      "Plasma Mobile update week 11 of 2020"
created_at: 2020-03-13 22:10:00 UTC+5.30
author:     Plasma Mobile team
layout:     post
---

In contrast to our usual bi-weekly blog format, this post wraps up only the most important changes that happened while no post was released. We are sorry for the lack of news in this period of time, but the good news is that as you will see in this post, the long break was not caused by a lack of content, but by a lack of time to write the post.

In the meantime, we held a sprint in Berlin, if you are interested in the improvements we made there, please see the [specific post](/2020/03/09/Plasma-Mobile-Sprint-2020.html).

![The Kalk app](https://invent.kde.org/cahfofpai/kalk/-/raw/develop/screenshots/screenshot%201.png){: .blog-post-image-right}

First of all we gained a couple of new apps!

Cahfofpai started the [calculator application Kalk](https://invent.kde.org/cahfofpai/kalk): He forked [Liri calculator](https://github.com/lirios/calculator) and ported it to Kirigami. The project is still a work in progress and contributions of all kinds are welcome.

[Kongress](https://invent.kde.org/kde/kongress), a companion application for conference attendees has also been added to the Plasma Mobile ecosystem. It provides practical information about conferences, showing information about the talks in various ways, e.g. in daily views, by talk category, etc. The user can also add conference talks to favorites and navigate to the web page of each talk.

### Plasma Camera

Plasma Camera, Plasma Mobile's default camera application, gained various improvements by Sebastian Pettke, who fixed video recording using cameras that don't allow setting FPS count.
As part of his work, he added a label which displays elapsed recording time.
Previously it was possible to trigger unexpected issues by switching the camera mode while recording, this is fixed now.
The choice of icons used all over the app was also improved, as well as error handling.

The area that previously already displayed the most recently taken photo gained support for showing a thumbnail for recorded videos.

In addition to that, the app now [highlights the currently active setting](https://invent.kde.org/kde/plasma-camera/commit/8cf5e618a9920f4475909f73f93c5c42e59da4fb).

### Calindori

Calindori now allows changing an event's start date, in case one picked the wrong date in the calendar initially.
Moreover, the flat "all events" page now uses a list view instead of a list of cards in order to improve user experience and performance.

With regards to more usability improvements, the global drawer actions have been separated into two sections: "active calendar" related (show monthly agenda, show events, show tasks) and "general" (create new, import, activate, delete other calendars). The actions of the active calendar are accessible with a single click - no need to display a new drawer page.

### Spacebar

Spacebar, the SMS and general telepathy client gained several improvements by the Telepathy maintainer, Alexander Akulich.
Its integrated contact search [is now case insensitive](https://invent.kde.org/kde/spacebar/commit/4f24ef5d8207584abf8d8e4c933121cf2afb5f59).
A very important part, starting new conversations was fixed: [Make it possible to start a new chat from NewConversation page](https://invent.kde.org/kde/spacebar/commit/db40180e311d86050672013febb3c1712fe932ea).
For telepathy backends that support it, Spacebar is now able to [show the contact's avatar picture in the conversation list](https://invent.kde.org/kde/spacebar/commit/e276d5da3f789e4e7b76c30e3880d3bf472eccf7).

Jonah Brüchert fixed [starting conversations with non-ktp contacts](https://invent.kde.org/kde/spacebar/-/commit/0e9f439470420a5344c17611924099e3087e739d), like typically required for SMS. Other chat protocols of course provide their own contact list, so they were not affected by this, but SMS requires local contacts.

Upstream in the KDE Telepathy library, various modernizations were done by Alexander Akulich and Jonah Brüchert.
Even more importantly, the additional patches to it that Spacebar depended on were upstreamed, so Spacebar doesn't require patching KTp anymore.

### Angelfish

![Screenshot of the updates angelfish](https://cdn.kde.org/screenshots/plasma-angelfish/homepage.png){: .blog-post-image }

Thanks to the work done by Rinigus Saar to enable installing Flatpak applications in SailfishOS, various Plasma Mobile apps now run on SailfishOS as well. Due to this development, Rinigus gained interest in contributing to Angelfish, our webbrowser, which is seemingly worth using on SailfishOS due to its better support for modern web standards than Jolla's default browser.
So Angelfish got more attention than usual:

To improve performance in scrollable list views, [Angelfish now uses a DelegateRecycler from Kirigami in most places](https://invent.kde.org/jbbgameich/plasma-angelfish/commit/2015ce46e841ac2f5ed12291d4bf49f5b1a0d77b).
Rinigus contributed a patch to [drop scrollbars on mobile](https://invent.kde.org/jbbgameich/plasma-angelfish/-/commit/c7409212e85027db6fa013ff291a1323a8ad3f71), which made angelfish not display the very desktop-like chromium scrollbars.
In order to help Angelfish to run on different platforms, translations with non-KDE styles were fixed by Jonah.

While debugging performance issues, Rinigus found out that interestingly the busy indicator used a significant amount of rendering power. We decided that using a busy indicator in browser navigation bars wasn't a modern design anyway, so we [removed it](https://invent.kde.org/jbbgameich/plasma-angelfish/-/commit/aeb22a069bb391478db5c921a22b0c529948f716)

To simplify the code and and use a less memory heavy approach for tabs, Rinigus [reimplemented the tabs view using a repeater](https://invent.kde.org/jbbgameich/plasma-angelfish/-/commit/71bea350eaf6107cd769b63e5b96f8adf577041a) instead of a list view.
He also  [refactored various pages to be shown as pages, not layers](https://invent.kde.org/jbbgameich/plasma-angelfish/-/commit/75f6473083d5fbe29f2ff30eafcfbe690802bc4f), which allows for using swipe gestures to navigate in the tabs view, settings and various other pages.
The tab switcher was afterwards redesigned by him.
As part of this effort, the tab thumbnails , which broke after one of the recent Qt updates, were fixed.

As a result of more work by Rinigus, [Angelfish now restores open tabs](https://invent.kde.org/jbbgameich/plasma-angelfish/-/commit/4f5732506d26b0dcdeac6361f3c8fa31fe276fe2) after restarts.

To enable automated unit testing and centralize the tab code in one place, Jonah rewrote the tab model, which was previously based on a mix of java script and QML, in c++.
The [address entry was moved into an overlay](https://invent.kde.org/jbbgameich/plasma-angelfish/-/commit/0635d5f790a7c9fdb1625406446740a6fd410aec) by Rinigus, to make more space for the text field.

Later Jonah [implemented various JavaScript dialogs](https://invent.kde.org/kde/plasma-angelfish/-/commit/6798537319a2208d3bd7dcae687ab1c657af1ae1), which are used by many websites to ask the user for text input or confirmations. Those dialogs are now handled in the application window, instead of QtWebEngine's default behaviour of opening a new window that hides the actual browser window.
He also [fixed visual inconsistencies in settings](https://invent.kde.org/kde/plasma-angelfish/-/commit/05281027b1b510acc9b4f637275447d095f6230f).

To improve private mode, Rinigus [disabled text prediction in private mode](https://invent.kde.org/kde/plasma-angelfish/-/commit/32bd5477d8e23b7a852cfd0577cb3d04978007bd).
In the second large refactoring project in the timespan covered by this post, he [rewrote the storage backend to use SQLite instead of JSON](https://invent.kde.org/kde/plasma-angelfish/-/commit/19d0fbab1d7265581fd47471e3c45c43ca6d5c11), which should improve loading, saving and especially filtering performance.
Tobias Fella soon after [fixed a crash that happend if the data location doesn't exist yet](https://invent.kde.org/kde/plasma-angelfish/-/commit/815e6b40f25cad31ea8d6a07947a446020a4d69a), which was unfortunately introduced by accident in the previous effort.

### Koko

As Qt seems to be reworking the QML engine internally in preparation for Qt 6, the scoping of properties changed in a way that made it necessary to [fix Koko's AlbumDelegate](https://cgit.kde.org/koko.git/commit/?id=249b400fe00cfe5d675977cfbf5a47446eda49f5). This restores thumbnails in the application.

### Plasma Phonebook

![Plasma Phonebook edit page](/img/screenshots/2020_w11_phonebook.png){: .blog-post-image-right }

Our contacts app saw an important fix, which restores the [functionality to remove phone numbers and instant messenger contacts (for real this time)](https://invent.kde.org/kde/plasma-phonebook/-/commit/e0f24ab2a644608b9386051f507568bf878ad581).
Jonah also made [scaling of contact pictures scale factor aware](https://invent.kde.org/kde/plasma-phonebook/-/commit/1bc3559ad4ef3efbea8b7447a9e4b415cc89bfb2).

This timespan's changes also contain a design improvement: [Text fields now fill all available width](https://invent.kde.org/kde/plasma-phonebook/-/commit/170edf0fca34b7a6419c1c17d47e120c8ccd1081).


### XDG-desktop-portal-kirigami

As a new feature, we started an xdg-desktop-portal implementation for Plasma Mobile, which is based on KDE's desktop implementation.
The Mobile variant already gained a mobile file and app chooser. You can find more about this work in the sprint blog post, but much of the work was already done before the sprint.

### Settings

Leinir [fixed the accounts module](https://phabricator.kde.org/D27454), and is currently working on [expanding and beautifing the module](https://phabricator.kde.org/D27681).

### Flatpak-kde-applications

KDE's repository of Flatpak applications saw many additions by the Plasma Mobile team:
Jonah added plasma-phonebook and fixed koko's failing build on the binary factory.
He also fixed thumbnails in the Koko Flatpak, which was missing kio-extras which contains the thumbnailer code.
Nicolas [contributed KTrip's](https://invent.kde.org/kde/flatpak-kde-applications/commit/bd95cbed603ad99ff15a3d81b49f4b2772d344fe) and calindori's manifest.
Jonah later packaged the Kirigami version of Okular as Flatpak, and Linus Jahn added his app PlasmaTube.
Dimitris recently [enabled building the Kongress app's Flatpak on the binary-factory](https://invent.kde.org/kde/flatpak-kde-applications/-/commit/1b9fe6480eabc43ac4c0e6f9ec3c44875b84beff).

### KWin Wayland

A patchset for enabling rotation in KWin/Wayland was merged into libKScreen, KScreen and KWin. You can find the complete patchset [on phabricator](https://phabricator.kde.org/D25907). Currently there's a small bug which prevents auto-rotation from working, and the option needs to be toggled manually. We hope to solve this in the upcoming days.

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.mastodon.host/videos/embed/0092b1d0-5031-44b6-aabf-d7c63f5fa84c?loop=1&muted=1" frameborder="0" allowfullscreen></iframe>

### Kirigami

Arjen Hiemstra added a [new component: ShadowedRectangle](https://phabricator.kde.org/D27984) in Kirigami. which is a rectangle with a shadow. It uses a custom shader that draws the rectangle and the shadow below it using distance fields, which has significantly better performance compared to the DropShadow effect. It also supports rounded corners, which the current Card code does not. It is planned to be used in current Card components in Kirigami.

### Announcements

This time we have a happy announcement to make: 
plasma-phone-components, which contains the most crucial parts of the Plasma Mobile GUI,
and its dependency plasma-nano are for the first time part of a Plasma release. The first Plasma release containing them is 5.18. We hope to enable distributions to start packaging them by this.

### Want to help?

This post shows what a difference new contributors can make. Do you want to be part of our team as well? Just in case we prepared [a few ideas for tasks new contributors](https://invent.kde.org/groups/kde/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Junior%20Job&label_name[]=Mobile) can work on.
